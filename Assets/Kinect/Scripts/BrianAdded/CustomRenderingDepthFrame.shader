﻿Shader "Custom/RenderingDepthFrame" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		nearCameraThreshold ("nearCameraThreshold", Range (0, 5000)) = 0
		farCameraThreshold ("farCameraThreshold", Range (2000, 5000)) = 4000


	}
	SubShader {
		Pass {

ZTest Always Cull Off ZWrite Off
Fog { Mode off }
			CGPROGRAM
			#include "UnityCG.cginc"
			#pragma vertex vert_img
			#pragma fragment frag
			#pragma enable_d3d11_debug_symbols
			#pragma only_renderers d3d11
			#pragma target 3.0

			#include "UnityCG.cginc"


			uniform sampler2D _MainTex;
			uniform float nearCameraThreshold;
			uniform float farCameraThreshold;
			float4 frag(v2f_img i) : COLOR {
				float2 uvs =  i.uv;
				uvs.x = 1.0 -uvs.x;
				float4 col = tex2D(_MainTex, uvs);
	
				float4 one = float4(1.0, 1.0, 1.0, 1.0);
				float4 zero = float4(0.0, 0.0, 0.0, 0.0);
				
				if(col.r > nearCameraThreshold && col.r < farCameraThreshold)
				return zero;//col.r / 65535.0 *15.0;
//					return zero;
				else return one;

			}
			ENDCG
		}

	}
}