﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;

/// <summary>
/// this pulls skeleton data from the sensor
/// </summary>
public class BodySourceManager : MonoBehaviour 
{
    private KinectSensor _Sensor;
    private BodyFrameReader _Reader;
    private Body[] _Data = null;
    private static BodySourceManager instance;
    public static BodySourceManager Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject go = new GameObject();
                go.name = "_BodySourceManager(Singleton)";
                instance = go.AddComponent<BodySourceManager>();
            }
            return instance;
        }
    }
    public Body[] GetData()
    {
        return _Data;
    }
    

    void Awake() 
    {
        //if it's null or if it's the right instance, don't destroy it.
        if (instance == null || instance == this)
            instance = this;
        else
        {
            DestroyImmediate(this);
        }
        _Sensor = KinectSensor.GetDefault();

        if (_Sensor != null)
        {
            _Reader = _Sensor.BodyFrameSource.OpenReader();
            
            if (!_Sensor.IsOpen)
            {
                _Sensor.Open();
            }
        }   
    }
    
    void Update () 
    {
        if (_Reader != null)
        {
            var frame = _Reader.AcquireLatestFrame();
            if (frame != null)
            {
                if (_Data == null)
                {
                    _Data = new Body[_Sensor.BodyFrameSource.BodyCount];
                }
                
                frame.GetAndRefreshBodyData(_Data);
                
                frame.Dispose();
                frame = null;
            }
        }    
    }
    
    void OnApplicationQuit()
    {
        if (_Reader != null)
        {
            _Reader.Dispose();
            _Reader = null;
        }
        
        if (_Sensor != null)
        {
            if (_Sensor.IsOpen)
            {
                _Sensor.Close();
            }
            
            _Sensor = null;
        }
    }
}
