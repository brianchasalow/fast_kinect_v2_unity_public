﻿	using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using System;

public class BodySourceView : MonoBehaviour 
{
	//6 prefabs are initialized into the body bank at start and are then disabled.
	public GameObject bodyPrefab;    
	public BodyUpdater[] BodyBank = new BodyUpdater[6];

	private Dictionary<ulong, BodyUpdater> _Bodies = new Dictionary<ulong, BodyUpdater>();

    public bool mapJointsToDepthMap = true;
	//the manager pulls raw data from the kinect API
    private BodySourceManager BodyManager;
    public GameObject BodyPrefabParents;

	/// <summary>
	/// this finds a body that is available to use.
	/// </summary>
	/// <returns>The free index.</returns>
	public int GetFreeIndex(){
		for (int i =0; i < BodyBank.Length; i++) {
			if(!BodyBank[i].gameObject.activeSelf)
				return i;
		}
		return 0;
	}

    public List<float> GetUsedBodyIndices()
    {
        List<float> returnList = new List<float>();
        foreach (KeyValuePair<ulong, BodyUpdater> updater in _Bodies)
        {
            returnList.Add(updater.Value.BodyIndexMaskID);
        }
        return returnList;
    }
    
    void Awake(){
        BodyManager = BodySourceManager.Instance;
		BodyBank = new BodyUpdater[6];
        if(BodyPrefabParents == null)
            BodyPrefabParents = new GameObject("BodyPrefabParents");
    
        float w = BodyIndexFrameSourceManager.Instance.FrameWidth;
        float h = BodyIndexFrameSourceManager.Instance.FrameHeight;
        BodyPrefabParents.transform.localScale = new Vector3(w / (float)h, 1.0f, 1.0f);


		//we can have up to 6 tracked bodies at a time, so create 6 at start.
        if (bodyPrefab == null)
        {
            Debug.Log("you need to assign a body prefab!");
            return;
        }


		for (int i = 0; i < 6; i++) {
			GameObject go = (GameObject)GameObject.Instantiate(bodyPrefab);
			BodyUpdater bodyUpdater = go.GetComponent<BodyUpdater>();
			bodyUpdater.InitWithPrefabIndex(i);
            bodyUpdater.mapJointsToDepthMap = mapJointsToDepthMap;
			BodyBank[i] = bodyUpdater;
			go.SetActive(false);
            go.transform.parent = BodyPrefabParents.transform;
            go.transform.localPosition = Vector3.zero;
            go.transform.localScale = Vector3.one;
			go.name = i + "_Body_Prefab";
		}
	}
    void Start()
    {
        if (mapJointsToDepthMap)
        {
            BodyIndexFrameView.Instance.bodyIndexRenderer.transform.parent = BodyPrefabParents.transform;
            BodyIndexFrameView.Instance.bodyIndexRenderer.transform.localScale = Vector3.one;
            BodyIndexFrameView.Instance.bodyIndexRenderer.transform.localPosition = Vector3.zero;
        }
    }
    void Update () 
    {
		//BodyManager is the script that polls for new data and grabs it into a Body[] array.
		if (BodyManager == null) return; 
        
		//get new data from the BodyManager
		Kinect.Body[] data = BodyManager.GetData();
        if (data == null) return;

		//these are the currently available ID's per this frame.
		List<ulong> trackedIds = new List<ulong>();    

        foreach(var body in data)
        {
			//if a body in the array is null, look at the rest of the array?
			if (body == null) continue;
             
            if(body.IsTracked)
            {
                trackedIds.Add (body.TrackingId);
            }
        }
        
        List<ulong> knownIds = new List<ulong>(_Bodies.Keys);
        
        // First delete untracked bodies
        foreach(ulong trackingId in knownIds)
        {
            if(!trackedIds.Contains(trackingId))
            {
                _Bodies[trackingId].gameObject.SetActive(false);
                _Bodies.Remove(trackingId);
            }
        }

		//update the tracked bodies= by this point the _Bodies array has all the valid bodies

        for (int i = 0; i < data.Length; i++)
        {
            var body = data[i];
            if (body == null) continue;

            if (body.IsTracked)
            {
                if (!_Bodies.ContainsKey(body.TrackingId))
                {
                    int newPrefabIndex = GetFreeIndex();
                    BodyBank[newPrefabIndex].gameObject.SetActive(true);
                    BodyBank[newPrefabIndex].BodyIndexMaskID = i;
                    _Bodies.Add(body.TrackingId, BodyBank[newPrefabIndex]);
                }

                _Bodies[body.TrackingId].RefreshBodyObject(body);
            }
        }
    }
    
    
 
    
 }